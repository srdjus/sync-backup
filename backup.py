#!/usr/bin/env python

from os import path, scandir
from shutil import copy2, copytree
from sys import argv


def find_index(data, condition):
    for i, val in enumerate(data):
        if condition(val):
            return (i, val)
    return (-1, None)


def compare(original_path, backup_path):
    for entry in scandir(original_path):

        # 'At' (entry index, entry)
        at = find_index(
            scandir(backup_path), 
            lambda x: True if x.name == entry.name else False)
        
        # File or directory exists 
        if at[0] > -1:

            # Dive into directory
            if entry.is_dir():
                compare(entry.path, path.join(backup_path, entry.name))

            # Compare 'last modified' dates (+1 for safety reasons)
            elif path.getmtime(entry.path) > path.getmtime(at[1].path) + 1:
                print("{:<20}".format("File updated:"), entry.path)
                copy2(entry.path, backup_path)
        
        # File or directory does not exist
        else:
            if entry.is_dir():
                print("{:<20}".format("New directory:"), entry.path)
                copytree(entry.path, path.join(backup_path, entry.name))
            else:
                print("{:<20}".format("New file:"), entry.path)
                copy2(entry.path, backup_path)


# ./filename.py /path/to/main /path/to/backup
compare(argv[1], argv[2]) 

print("Synced")
