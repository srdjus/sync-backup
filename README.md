# Sync backup

Script I wrote so I can easily create and update backups of my important files. Lot of things missing, but basic functionality is already there.

### Usage
Script uses Python 3 and it works on Linux. I haven't tested it on other operating systems yet. 

```sh
$ ./backup.py /path/to/original/directory /path/to/backup/directory
```

### How
Manually create backup directory (it'll work with non-empty directories too, but I don't recommend it, files will be overwritten). It should look something like this:

```
/path/to/backup/directory
```

First time you run the command, all files and directories will be copied from original directory to your backup directory. Second and any other time you run the command (with same original and backup path) any changes you made inside the original directory changes will be applied to your backup directory. **Only updated and newly created files and folder will be copied to your backup directory, rest of files will be left untouched.**

You should only make changes inside your original directory. Because, making changes inside your backup directory will modify file informations so script won't be able to update it properly on next scan. 

I will handle exceptions later.
